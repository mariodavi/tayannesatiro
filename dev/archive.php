<?php /* Template Name: Arquivos */ ?>
<?php get_header(); ?>        

        <!-- Titulo page / Parallax-->
        <section class="section section-height-800 parallax-container context-dark bg-gray-darkest text-xl-left" data-parallax-img="<?php bloginfo('template_url') ?>/images/backgrounds/bg-nutricionista.jpg">
          <div class="parallax-content">
            <div class="bg-overlay-black">
              <div class="container section-30 section-md-95 section-lg-top-120 section-lg-bottom-150">
                <div class="d-none d-lg-block">
                  <h1>Arquivos</h1>
                </div>
                <!-- List Inline-->
                <ul class="list-inline list-inline-dashed list-white text-big p offset-md-top-13">
                  <li><a href="<?php bloginfo('url'); ?>">Home</a></li>
                  <li>Arquivos
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <!-- //Titulo page / Parallax-->

        <!-- Blog Posts -->
        <section class="section-95 section-md-bottom-120 text-left">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-md-10 col-lg-8">
                <?php $p=0; if (have_posts()) : while(have_posts()) : the_post();
                $altimg = get_the_title(); if (($p % 2) == 0) { ?>
                <!-- Post Classic-->
                <article class="post-classic"><a class="thumbnail-zoom" href="<?php the_permalink(); ?>"><span class="thumbnail-zoom-img-wrap">
                  <?php the_post_thumbnail( 'post-img', array('class' => 'img-fluid center-block', 'altimg' => $altimg)); ?>
                  </span></a>
                  <div class="post-body">
                    <p class="post-title text-big font-weight-bold text-uppercase text-spacing-100 offset-top-20"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                    <!-- List Inline-->
                    <ul class="list-inline list-inline-22 list-inline-dashed-vertical font-weight-bold p">
                      <li><span class="icon icon-xs material-icons-ico material-icons-event text-middle text-gray"></span><span class="text-middle inset-left-7 post-meta"><?php the_time('j \d\e\ F, Y'); ?></span></li>
                      <?php $username = get_userdata( $post->post_author ); ?>
                      <li><span class="icon icon-xs material-icons-ico material-icons-person text-middle text-gray"></span><span class="text-regular inset-left-4 text-gray text-middle">Por:</span><a class="text-middle link-decoration-none text-hover-primary text-gray-light inset-left-4" href="my-philosophy.html"><?php the_author(); ?></a></li>
                    </ul>
                    <p class="offset-top-20"><?php the_excerpt(); ?></p>
                    <div class="group-sm offset-top-30"><?php the_tags("", ", ", ""); ?>
                    </div>
                  </div>
                </article>
                <?php } else { ?>
                <div class="offset-top-50 offset-md-top-70">
                  <!-- Post Classic-->
                  <article class="post-classic"><a class="thumbnail-zoom" href="<?php the_permalink(); ?>"><span class="thumbnail-zoom-img-wrap">
                  <?php the_post_thumbnail( 'post-img', array('class' => 'img-fluid center-block', 'altimg' => $altimg)); ?>
                  </span></a>
                    <div class="post-body">
                      <p class="post-title text-big font-weight-bold text-uppercase text-spacing-100 offset-top-20"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                      <!-- List Inline-->
                      <ul class="list-inline list-inline-22 list-inline-dashed-vertical font-weight-bold p">
                        <li><span class="icon icon-xs material-icons-ico material-icons-event text-middle text-gray"></span><span class="text-middle inset-left-7 post-meta"><?php the_time('j \d\e\ F, Y'); ?></span></li>
                        <?php $username = get_userdata( $post->post_author ); ?>
                        <li><span class="icon icon-xs material-icons-ico material-icons-person text-middle text-gray"></span><span class="text-regular inset-left-4 text-gray text-middle">Por:</span><a class="text-middle link-decoration-none text-hover-primary text-gray-light inset-left-4" href="my-philosophy.html"><?php the_author(); ?></a></li>
                      </ul>
                      <p class="offset-top-20"><?php the_excerpt(); ?></p>
                      <div class="group-sm offset-top-30"><a class="btn btn-tag btn-primary-gray-outline" href="#"><?php the_tags("", ", ", ""); ?></a>
                      </div>
                    </div>
                  </article>
                </div>
                <?php  } $p++; endwhile; else : ?>
                <div class="col-md-10 col-lg-8">
                  <article class="post-classic">
                      <p class="post-title text-big font-weight-bold text-uppercase text-spacing-100 offset-top-20"><?php _e('Não há posts publicados!');?></p>
                  </article>
                </div>
                <?php endif; wp_reset_query(); ?>
                
                <!-- paginacao -->
                <div class="text-center offset-top-50 offset-md-top-115">
                  <!-- Pagination Classic-->
                  <nav>
                    <ul class="pagination-classic">
                      <li class="pagination-classic-prev"><a href="#">Prev</a></li>
                      <li class="d-none d-sm-inline-block"><a href="#">1</a></li>
                      <li class="d-none d-sm-inline-block"><a href="#">2</a></li>
                      <li class="active"><a href="#">3</a></li>
                      <li class="d-none d-md-inline-block"><a href="#">4</a></li>
                      <li class="pagination-classic-next"><a href="#">Next</a></li>
                    </ul>
                  </nav>
                </div>
                <!-- //paginacao -->
              </div>
              
              <!-- sidebar -->
              <div class="col-md-10 col-lg-4 offset-top-90 offset-md-top-0">
                <?php get_sidebar(); ?>
              </div>
              <!-- //sidebar -->

            </div>
          </div>
        </section>

<?php get_footer(); ?>