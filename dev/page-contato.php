<?php /* Template Name: Contato */ ?>
<?php get_header(); the_post(); ?>

        <!-- Titulo page / Parallax-->
        <?php include('include/page-title.php'); ?>
        <!-- //Titulo page / Parallax-->

        <!-- Contact Info-->
        <section class="section-95 section-md-bottom-80">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-md-10">
                <h1>Me envia uma mensagem</h1>
              </div>
            </div>
            <div class="row justify-content-sm-center offset-top-34">
              <div class="col-md-10 col-xl-6">
                <?php 
                $message = 0;

                if(!empty($_POST['form-contato'])) {
                  $nome = $_POST['nome'];
                  $email = $_POST['email'];
                  $telefone = $_POST['telefone'];
                  $mensagem =  $_POST['mensagem'];

                  wp_mail( 'contato@tayannesatiro.com.br, nutri@tayannesatiro.com.br', 'Contato do site - Tayanne Sátiro',"
                  <strong>Nome:</strong> {$nome}<br><br>
                  <strong>Email:</strong> {$email}<br><br>
                  <strong>Telefone:</strong> {$telefone}<br><br>
                  <strong>Mensagem:</strong> {$mensagem}<br><br>
                  ", array('From: Site Tayanne Sátiro <contato@tayannesatiro.com.br>','Content-Type: text/html; charset=UTF-8'), array() );

                  $message = 'Mensagem enviada com sucesso!';
                } ?>
                <!-- RD Mailform-->
                <form class="rd-mailform text-left offset-top-20" method="post" id="form-contato" action="<?php bloginfo('url') ?>/contato/#resposta">
                  <input type="hidden" name="form-contato" value="1">
                  <?php if($message) : ?>
                  <div id="resposta" class="alert alert-success alert-dismissible fade show text-center" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <h5><strong><?php echo $message; ?></strong></h5>
                  </div>
                  <?php endif; ?>
                  <div class="row justify-content-sm-center">
                    <div class="col-sm-6 inset-sm-right-7">
                      <div class="form-wrap">
                        <label class="form-label" for="forms-contact-us-first-name">Nome</label>
                        <input class="form-input" id="forms-contact-us-first-name" type="text" name="nome" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="col-sm-6 offset-top-10 offset-xs-top-0 inset-sm-left-7">
                      <div class="form-wrap">
                        <label class="form-label" for="forms-contact-us-phone">Telefone</label>
                        <input class="form-input" id="forms-contact-us-phone" type="text" name="telefone" data-constraints="@Numeric @Required">
                      </div>
                    </div>
                    <div class="col-sm-12 offset-top-10">
                      <div class="form-wrap">
                        <label class="form-label" for="forms-contact-us-email">E-mail</label>
                        <input class="form-input" id="forms-contact-us-email" type="email" name="email" data-constraints="@Email @Required">
                      </div>
                    </div>
                    <div class="col-sm-12 offset-top-10 text-center text-xl-left">
                      <div class="form-wrap">
                        <label class="form-label" for="forms-contact-us-message">Mensagem</label>
                        <textarea class="form-input" id="forms-contact-us-message" name="mensagem" data-constraints="@Required"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="text-center text-lg-right offset-top-10">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>

<?php get_footer(); ?>