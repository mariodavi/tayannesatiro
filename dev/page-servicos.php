<?php /* Template Name: Serviços */ ?>
<?php get_header(); the_post(); ?>

      <!-- Titulo page / Parallax-->
      <?php include('include/page-title.php'); ?>
        <!-- //Titulo page / Parallax-->

        <!-- Welness-->
        <section class="section-95">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-md-10 col-lg-12 col-xl-10">
                <?php /*<h3> the_title(); </h3>*/ ?>
                <?php the_content(); ?>
                <div class="offset-top-25">
                <?php $i=0; if (have_rows('servicos')) : while (have_rows('servicos')) : $i++; the_row(); ?>
                <div class="card-wrap" id="servico-<?php echo $i; ?>">
                  <div class="card card-servicos">
                    <h6 class="card-header"><?php the_sub_field('servico-titulo'); ?></h6>
                    <div class="card-body">
                      <p class="card-text"><?php the_sub_field('servico-info'); ?></p>
                    </div>
                  </div>
                </div>
                <?php endwhile; endif; ?> 
                  <!-- Accordion-->
                  <?php /*$i=0; if (have_rows('servicos')) : ?>
                  <div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php while (have_rows('servicos')) : $i++;the_row(); ?>
                    <div class="panel panel-custom panel-default">
                      <div class="panel-heading" id="heading-<?php echo $i; ?>" role="tab">
                        <p class="panel-title"><a class="d-block collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $i; ?>"><?php the_sub_field('servico-titulo'); ?></a></p>
                      </div>
                      <div class="panel-collapse collapse" id="collapse-<?php echo $i; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $i; ?>">
                        <div class="panel-body"><?php the_sub_field('servico-info'); ?></div>
                      </div>
                    </div>
                    <?php endwhile; ?>                    
                  </div>
                  <?php endif;*/ ?>
                </div>
              </div>
            </div>
          </div>
        </section>

<?php get_footer(); ?>