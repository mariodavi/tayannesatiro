<?php /* Template Name: Sobre */ ?>
<?php get_header(); the_post(); ?>

      <!-- Parallax-->
      <?php include('include/page-title.php'); ?>
      <!-- Page Content-->
      <main class="page-content">
        <!-- My Philosophy-->
        <section class="section-95">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-md-10 col-lg-6">
                <div class="d-inline-block">
                  <?php the_post_thumbnail( 'post-img', array('class' => 'img-fluid center-block', 'altimg' => $altimg)); ?>
                  <!-- Box-->
                  <?php query_posts('pagename=contato'); the_post(); ?>
                  <div class="box-xs bg-gray-lighter">
                    <!-- List Inline-->                    
                    <ul class="list-inline list-inline-12 p d-inline-block text-left">
                      <li><a class="link-decoration-none text-black-06" href="tel:<?php the_field('contato-tel'); ?>"><span class="icon icon-xs material-design-ico material-design-phone370 text-middle text-primary"></span><span class="text-hover-primary text-black-06 text-middle inset-left-10"><?php the_field('contato-tel'); ?></span></a></li>
                      <li><a class="link-decoration-none text-black-06" href="#"><span class="icon icon-xs material-design-ico material-design-write20 text-middle text-primary"></span><span class="text-hover-primary text-black-06 text-middle inset-left-10"><span class="__cf_email__ reverse"><?php the_field('contato-email'); ?></span></span></a></li>                      
                    </ul>
                  </div>
                  <?php wp_reset_query(); ?>

                  <?php query_posts('pagename=servicos'); the_post(); ?>
                  <div class="servicos card-servicos mt-5">
                    <h2>Serviços</h2>
                    <p class="my-4">Clique nos serviços abaixo e saiba mais.</p>
                    <?php $i=0; if (have_rows('servicos')) : while (have_rows('servicos')) : $i++; the_row(); ?>
                    <span class="font-weight-bold text-primary d-block mb-2"><a href="<?php bloginfo('url'); ?>/servicos#servico-<?php echo $i; ?>" title="<?php the_sub_field('servico-titulo'); ?>">- <?php the_sub_field('servico-titulo'); ?></a></span>
                    <?php endwhile; endif; wp_reset_query(); ?> 
                  </div>
                  <?php wp_reset_query(); ?>
                </div>
              </div>
              <div class="col-md-10 col-lg-6 offset-top-40 offset-md-top-0 text-left">
                <div class="inset-xl-left-15">
                  <?php the_content(); ?>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Ready to learn all the secrets to a vibrant health?-->
        <!-- Parallax-->

        <section class="section section-height-800 parallax-container" data-parallax-img="<?php the_field('ebook-bg') ?>">
          <div class="parallax-content">
            <div class="container section-95">
              <div class="row justify-content-sm-center justify-content-xl-end">
                <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6">
                  <!-- Box-->
                  <div class="box-sm bg-default">
                    <h1 class="text-primary"><?php the_field('ebook-titulo') ?></h1>
                    <p class="text-big"><?php the_field('ebook-info') ?></p><a class="btn btn-primary offset-top-13 offset-lg-top-34" href="<?php the_field('ebook-link') ?>"><?php the_field('ebook-btntitulo') ?></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </main>

<?php get_footer(); ?>