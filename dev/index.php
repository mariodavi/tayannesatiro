<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
	<?php query_posts('pagename=home'); the_post(); ?>
	
    <?php include 'include/slide.php'; ?>
       
        <!-- sobre -->
        <section class="section-95">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-md-8 col-lg-6 order-lg-1"><img class="img-fluid center-block" src="<?php the_field('sobre-foto'); ?>" alt="Tayanne Sátiro"></div>
              <div class="col-md-8 col-lg-6 offset-top-34 offset-md-top-0">
                <div class="inset-lg-left-50 inset-lg-right-50 inset-xl-left-50 inset-xl-right-50">
                  <h1>Tayanne Sátiro</h1>
                  <?php the_field('sobre'); ?>
                  <a class="btn btn-primary offset-top-34" href="<?php bloginfo('url') ?>/sobre">Saiba mais</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        
        <!-- Parallax-->
        <section class="section section-height-800 parallax-container" data-parallax-img="<?php the_field('rede-foto'); ?>">
          <div class="parallax-content">
            <div class="container section-110">
              <div class="row justify-content-sm-center">
                <div class="col-sm-10 col-md-8">
                  <!-- Box-->
                  <div class="box-sm bg-default">
                    <h1 class="text-primary"><?php the_field('rede-titulo'); ?></h1>
                    <p class="text-big text-gray offset-top-13"><?php the_field('rede-info'); ?></p><a class="btn btn-primary offset-top-13" href="<?php the_field('rede-link'); ?>" target="_blank"><?php the_field('rede-botao'); ?></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
		<?php wp_reset_query(); ?>

        <!-- Contact Us-->        
        <section class="section-95 section-md-bottom-45">
          <div class="container">
            <div class="row justify-content-sm-center justify-content-lg-start text-lg-left">
              <?php query_posts('pagename=contato'); the_post(); ?>
              <div class="col-md-10 col-lg-3">
                <h6>Endereço</h6>
                <!-- Contact Info-->
                <address class="contact-info p offset-top-20">
                  <p><?php the_field('contato-end'); ?></p>
                  <dl>
                    <dt class="mr-2"><b>Telefones:</b></dt>
                   
                    <dd><a class="text-gray-light link-decoration-none text-hover-primary" href="tel:<?php the_field('contato-tel'); ?>"><?php the_field('contato-tel'); ?></a></dd>
                  </dl>
                  <dl class="offset-top-10">
                    <dt class="mr-2"><b>E-mail:</b></dt>
                    <dd><a class="link-decoration-none" href=""><span class="__cf_email__ reverse"><?php the_field('contato-email'); ?></span></a></dd>
                  </dl>
                </address>
              </div>
              <?php wp_reset_query(); ?>
              <div class="col-md-10 col-lg-5 offset-top-40 offset-md-top-0">
                <h6>Me envia uma mensagem</h6>
                <?php 
                $message = 0;

                if(!empty($_POST['form-contato'])) {
                  $nome = $_POST['nome'];
                  $email = $_POST['email'];
                  $telefone = $_POST['telefone'];
                  $mensagem =  $_POST['mensagem'];

                  wp_mail( 'contato@tayannesatiro.com.br, nutri@tayannesatiro.com.br', 'Contato do site - Tayanne Sátiro ',"
                  <strong>Nome:</strong> {$nome}<br><br>
                  <strong>Email:</strong> {$email}<br><br>
                  <strong>Telefone:</strong> {$telefone}<br><br>
                  <strong>Mensagem:</strong> {$mensagem}<br><br>
                  ", array('From: Site Tayanne Sátiro <contato@tayannesatiro.com.br>','Content-Type: text/html; charset=UTF-8'), array() );

                  $message = 'Mensagem enviada com sucesso!';
                } ?>
                <!-- RD Mailform-->
                <form class="rd-mailform text-left offset-top-20" method="post" id="form-contato" action="./#resposta">
                  <div class="row justify-content-sm-center">
                    <div class="col-sm-6 inset-sm-right-7">
                      <input type="hidden" name="form-contato" value="1">
                      <?php if($message) : ?>
                      <div id="resposta" class="alert alert-success alert-dismissible fade show text-center" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                      <h5><strong><?php echo $message; ?></strong></h5>
                      </div>
                      <?php endif; ?>
                      <div class="form-wrap">
                        <label class="form-label" for="contact-us-first-name">Nome</label>
                        <input class="form-input" id="contact-us-first-name" type="text" name="nome" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="col-sm-6 offset-top-10 offset-xs-top-0 inset-sm-left-7">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-us-phone">Telefone</label>
                        <input class="form-input" id="contact-us-phone" type="text" name="telefone" data-constraints="@Numeric @Required">
                      </div>
                    </div>
                    <div class="col-sm-12 offset-top-10">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-us-email">E-mail</label>
                        <input class="form-input" id="contact-us-email" type="email" name="email" data-constraints="@Email @Required">
                      </div>
                    </div>
                    <div class="col-sm-12 offset-top-10 text-center text-xl-left">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-us-message">Mensagem</label>
                        <textarea class="form-input" id="contact-us-message" name="mensagem" data-constraints="@Required"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="text-center text-lg-right offset-top-10">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>

<?php get_footer(); ?>