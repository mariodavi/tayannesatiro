<?php get_header(); the_post(); ?>        

        <!-- Titulo page / Parallax-->
        <section class="section section-height-800 parallax-container context-dark bg-gray-darkest text-xl-left" data-parallax-img="<?php bloginfo('template_url') ?>/images/backgrounds/bg-nutricionista.jpg">
          <div class="parallax-content">
            <div class="bg-overlay-black">
              <div class="container section-30 section-md-95 section-lg-top-120 section-lg-bottom-150">
                <div class="d-none d-lg-block">
                  <h1>Blog</h1>
                </div>
                <!-- List Inline-->
                <ul class="list-inline list-inline-dashed list-white text-big p offset-md-top-13">
                  <li><a href="<?php bloginfo('url'); ?>">Home</a></li>
                  <li>Blog
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <!-- //Titulo page / Parallax-->

        <!-- Blog Posts -->
        <section class="section-95 section-md-bottom-120 text-left">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-md-10 col-lg-8">
                <?php $altimg = get_the_title(); ?>
                <!-- Post Classic-->
                <article class="post-classic"><a class="thumbnail-zoom" href="<?php the_permalink(); ?>"><span class="thumbnail-zoom-img-wrap">
                  <?php the_post_thumbnail( 'post-img', array('class' => 'img-fluid center-block', 'altimg' => $altimg)); ?>
                  </span></a>
                  <div class="post-body"> 
                    <!-- titulo-post / compartilhe -->
                    <?php include('include/posts-share.php'); ?>
                    <!-- //titulo-post / compartilhe -->
                    <div class="post-content offset-top-20"><?php the_content(); ?></div>
                    <div class="group-sm offset-top-30"><?php the_tags("", ", ", ""); ?>
                    </div>
                  </div>
                </article>
              </div>
              
              <!-- sidebar -->
              <div class="col-md-10 col-lg-4 offset-top-90 offset-md-top-0">
                <?php get_sidebar(); ?>
              </div>
              <!-- //sidebar -->

            </div>
          </div>
        </section>

<?php get_footer(); ?>