<?php /* Template Name: Blog */ ?>
<?php get_header(); ?>        

        <!-- Titulo page / Parallax-->
        <section class="section section-height-800 parallax-container context-dark bg-gray-darkest text-xl-left" data-parallax-img="<?php bloginfo('template_url') ?>/images/backgrounds/bg-nutricionista.jpg">
          <div class="parallax-content">
            <div class="bg-overlay-black">
              <div class="container section-30 section-md-95 section-lg-top-120 section-lg-bottom-150">
                <div class="d-none d-lg-block">
                  <h1><?php single_cat_title(); ?></h1>
                </div>
                <!-- List Inline-->
                <ul class="list-inline list-inline-dashed list-white text-big p offset-md-top-13">
                  <li><a href="<?php bloginfo('url'); ?>">Home</a></li>
                  <li><?php single_cat_title(); ?>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <!-- //Titulo page / Parallax-->

        <!-- Blog Posts -->
        <section class="section-95 section-md-bottom-120 text-left">
          <div class="container">
            <div class="row justify-content-sm-center">
              <div class="col-md-10 col-lg-8">
                <?php $p=0; if (have_posts()) : while(have_posts()) : the_post();
                $altimg = get_the_title(); if (($p % 2) == 0) { ?>
                <!-- Post Classic-->
                <article class="post-classic"><a class="thumbnail-zoom" href="<?php the_permalink(); ?>"><span class="thumbnail-zoom-img-wrap">
                  <?php the_post_thumbnail( 'post-img', array('class' => 'img-fluid center-block', 'altimg' => $altimg)); ?>
                  </span></a>
                  <div class="post-body">
                    <!-- titulo-post / compartilhe -->
                    <?php include('include/posts-share.php'); ?>
                    <!-- //titulo-post / compartilhe -->
                    
                    <p class="offset-top-20"><?php the_excerpt(); ?></p>
                    <div class="group-sm offset-top-30"><?php the_tags("", ", ", ""); ?>
                    </div>
                  </div>
                </article>
                <?php } else { ?>
                <div class="offset-top-50 offset-md-top-70">
                  <!-- Post Classic-->
                  <article class="post-classic"><a class="thumbnail-zoom" href="<?php the_permalink(); ?>"><span class="thumbnail-zoom-img-wrap">
                  <?php the_post_thumbnail( 'post-img', array('class' => 'img-fluid center-block', 'altimg' => $altimg)); ?>
                  </span></a>
                    <div class="post-body">
                      <?php include('include/posts-share.php'); ?>
                      <p class="offset-top-20"><?php the_excerpt(); ?></p>
                      <div class="group-sm offset-top-30"><a class="btn btn-tag btn-primary-gray-outline" href="#"><?php the_tags("", ", ", ""); ?></a>
                      </div>
                    </div>
                  </article>
                </div>
                <?php  } $p++; endwhile; else : ?>
                <div class="col-md-10 col-lg-8">
                  <article class="post-classic">
                      <p class="post-title text-big font-weight-bold text-uppercase text-spacing-100 offset-top-20"><?php _e('Não há posts publicados!');?></p>
                  </article>
                </div>
                <?php endif; wp_reset_query(); ?>
                
                <!-- paginacao -->
                <div id="paginacao" class="text-center offset-top-50 offset-md-top-115">
                  <?php wp_pagenavi(); ?>
                </div>
                <!-- //paginacao -->
              </div>
              
              <!-- sidebar -->
              <div class="col-md-10 col-lg-4 offset-top-90 offset-md-top-0">
                <?php get_sidebar(); ?>
              </div>
              <!-- //sidebar -->

            </div>
          </div>
        </section>

<?php get_footer(); ?>