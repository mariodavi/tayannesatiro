<?php /* Template Name: Links */
define('WP_USE_THEMES', false);
// require("./wp-blog-header.php");
include("../../../wp-blog-header.php");
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php if (function_exists('is_tag') && is_tag()) { echo 'Posts tagged &quot;'.$tag.'&quot; - '; } elseif (is_archive()) { wp_title(''); echo ' - '; } elseif (is_search()) { echo 'Procurando por &quot;'.esc_html($s).'&quot; - '; } elseif (!(is_404()) && (is_single()) || (is_page())) { wp_title(''); echo ' - '; } elseif (is_404()) { echo 'Nada encontrado - '; } bloginfo('name'); ?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url') ?>/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url') ?>/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url') ?>/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url') ?>/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url') ?>/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url') ?>/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url') ?>/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url') ?>/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url') ?>/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_url') ?>/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url') ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_url') ?>/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url') ?>/images/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url') ?>/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo('template_url') ?>/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="icon" href="<?php bloginfo('template_url') ?>/images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&family=Oregano&display=swap" rel="stylesheet">
    <!--[if lt IE 8]>
    <p class="browserupgrade">Você está utilizando um navegador <strong>desatualizado</strong>. Favor <a href="https://browsehappy.com/?locale=pt-br">atualizar seu navegador</a> para melhorar sua experiência.</p>
    <![endif]-->
    <style>
        html, body {
            height: 100%;
            color: #fff;
            text-align: center;
            position: relative;
        }
        body {
            /*background: rgb(212,99,163);
            background: linear-gradient(180deg, rgba(212,99,163,1) 0%, rgba(248,137,111,1) 100%);
            background-repeat: repeat-x;*/
            
        }
        .wrap {
            background: rgb(236,93,55);
            background: linear-gradient(0deg, rgba(236,93,55,1) 0%, rgba(250,172,106,1) 100%);
            background-repeat: repeat-x;
            //height: inherit;
        }
        /*@media (min-width:562px) {
            .wrap {
                height: inherit;
            }
        }*/
         a {
            text-decoration: none;
            color: #fff;
         }
         a:hover {
            text-decoration: none;
            color: #ec5d37;
        }
        .links-item {
            display: block;
            border: 2px solid #fff;
            color: #fff;
            padding: 1rem;
            font-weight: 600;
            margin: 1rem 0 0;
        }
        .links-item:hover {
            background: #fff;
            color: #ec5d37;
        }
        .rounded-circle {
            border: 5px solid #fff;
            transition:all 0.2s ease-in-out
        }
        .rounded-circle:hover {
            box-shadow: none!important;
            transition:all 0.2s ease-in-out
        }
    </style>
  </head>

  <body>
      <div class="wrap">
        <div class="container py-5">
            <div class="row">
                <div class="col-sm-8 mx-auto">
                    <a href="<?php bloginfo('url'); ?>" title="Tayanne Sátiro - Nutricionista"><img src="<?php bloginfo('template_url')?>/images/logo-134x42.png" class="d-block mx-auto" alt="Tayanne Sátiro – Nutricionista" /></a>
                    <a href="https://www.instagram.com/tayannesatiro/" class="d-inline-block my-4" title="Tayanne Sátiro" target="_blank"><img src="<?php bloginfo('template_url')?>/images/tayanne-satiro.jpg" class="rounded-circle shadow" alt="Tayanne Sátiro - Nutricionista" /></a>
                    <h6><a href="https://www.instagram.com/tayannesatiro/" title="Tayanne Sátiro - Nutricionista" target="_blank">@tayannesatiro</a></h6>
                    <?php if (have_rows('links')) : ?>
                    <div class="links mt-5">
                        <a href="http://api.whatsapp.com/send?1=pt_BR&amp;phone=5583988858050&amp;text=Olá Tayanne, gostaria de fazer o agendamento para um consulta" class="links-item" target="_blank" rel="noopener">AGENDAMENTO ON-LINE</a>
                        <?php while (have_rows('links')) :  the_row(); ?>
                        <a href="<?php the_sub_field('links-link'); ?>" class="links-item"><?php the_sub_field('links-titulo'); ?></a>
                        <?php endwhile; ?> 
                    </div>                
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="pb-3"><p>Tayanne Sátiro © <?php echo date('Y'); ?></p></div>
      </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    
    <!-- Global site tag (gtag.js) - Google Ads: 667455730 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-667455730"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-667455730'); </script>
    <!-- Event snippet for Website sale conversion page --> <script> gtag('event', 'conversion', { 'send_to': 'AW-667455730/o8QaCK_RyNMBEPKhor4C', 'transaction_id': '' }); </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163824760-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-163824760-1');
    </script>
  </body>
</html>