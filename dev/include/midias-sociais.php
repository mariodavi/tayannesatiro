                    <?php query_posts('pagename=contato'); the_post(); ?>
                    <ul class="list-inline list-inline-0">
                      <?php if (get_field('contato-insta')) : ?>
                      <li class="text-center"><a class="icon icon-xxs icon-circle icon-gray-lighter fa fa-instagram" href="<?php the_field('contato-insta'); ?>" target="_blank"></a></li>
                      <?php endif; if (get_field('contato-youtube')) : ?>
                      <li class="text-center"><a class="icon icon-xxs icon-circle icon-gray-lighter fa fa-youtube-play" href="<?php the_field('contato-youtube'); ?>" target="_blank"></a></li>
                      <?php endif; if (get_field('contato-face')) : ?>
                      <li class="text-center"><a class="icon icon-xxs icon-circle icon-gray-lighter fa fa-facebook" href="<?php the_field('contato-face'); ?>" target="_blank"></a></li>
                      <?php endif; ?>
                    </ul>
                    <?php wp_reset_query(); ?>