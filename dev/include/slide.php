      <!-- Swiper-->
      <?php if (have_rows('painel')) : ?>
      <div class="painel">
        <?php while(have_rows('painel')) : the_row();
        $painel_img = get_sub_field('painel-img');
        $posts = get_sub_field('painel-linkint');
        if( $posts ) {
        foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <div class="painel-item"><a href="<?php the_permalink(); ?>"><img src="<?php the_sub_field('painel-img'); ?>" class="img-fluid center-block"></a></div>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
        <?php } elseif(get_sub_field('painel-linkext')) { ?>
          <div class="painel-item"><a href="<?php the_sub_field('painel-linkext'); ?>" target="blank_"><img src="<?php the_sub_field('painel-img'); ?>" class="img-fluid center-block"></a></div>
        <?php } else { ?>
          <div class="painel-item"><img src="<?php the_sub_field('painel-img'); ?>" class="img-fluid center-block"></div>
        <?php } endwhile; endif; ?>  
      </div>