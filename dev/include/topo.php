      <!-- Page Header-->
      <header class="page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-default" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="90px" data-lg-stick-up-offset="90px">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                <div class="rd-navbar-panel-inner">
                  <div class="rd-navbar-panel-inner-left">
                    <!-- RD Navbar Brand-->
                    <div class="rd-navbar-brand"><a class="brand-name" href="<?php bloginfo('url'); ?>"><img class="img-fluid center-block logo-mobile" src="<?php bloginfo('template_url') ?>/images/logo-134x42.png" alt="<?php bloginfo('blog_name') ?>"></a></div>
                  </div>
                  
                  <div class="rd-navbar-panel-inner-right">
                    <!-- midias sociais-->
                    <?php include('midias-sociais.php'); ?>
                  </div>
                  
                </div>
                <!-- RD Navbar Collaps-->
                <button class="rd-navbar-collapse-toggle" data-rd-navbar-toggle=".rd-navbar-panel-inner-right"><span></span></button>
              </div>
              <div class="rd-navbar-nav-wrap">
                <!-- RD Navbar Nav-->
                <?php
                wp_nav_menu( array(
                  'menu' => 'Menu',
                  'menu_class' => 'rd-navbar-nav',
                )); ?>
              </div>
            </div>
          </nav>
        </div>
      </header>