<div class="posts-share">
                      <p class="post-title text-big font-weight-bold text-uppercase text-spacing-100 offset-top-20"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                      <!-- List Inline-->
                      <ul class="list-inline list-inline-22 list-inline-dashed-vertical font-weight-bold p">
                        <li><span class="icon icon-xs material-icons-ico material-icons-event text-middle text-gray"></span><span class="text-middle inset-left-7 post-meta"><?php the_time('j \d\e\ F, Y'); ?></span></li>
                        <?php $username = get_userdata( $post->post_author ); ?>
                        <li><span class="icon icon-xs material-icons-ico material-icons-person text-middle text-gray"></span><span class="text-regular inset-left-4 text-gray text-middle">Por:</span><a class="text-middle link-decoration-none text-hover-primary text-gray-light inset-left-4" href="my-philosophy.html"><?php the_author(); ?></a></li>
                        <li><span class="icon icon-xs material-icons-ico material-icons-share text-middle text-gray"></span> <span class="text-regular inset-left-4 text-gray text-middle">Compartilhe:</span></li>
                        <li>
                          <!-- AddToAny -->
                          <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <!-- <a class="a2a_dd" href="https://www.addtoany.com/share"></a> -->
                            <a class="a2a_button_whatsapp"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_linkedin"></a>
                            <a class="a2a_button_email"></a>
                          </div>
                        </li>
                      </ul>
                    </div>