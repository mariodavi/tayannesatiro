<section class="section section-height-800 parallax-container context-dark bg-gray-darkest text-xl-left" data-parallax-img="<?php the_field('titulo-bg'); ?>"> 
          <div class="parallax-content">
            <div class="bg-overlay-black">
              <div class="container section-30 section-md-95 section-lg-top-120 section-lg-bottom-150">
                <div class="d-none d-lg-block">
                  <h1><?php the_title(); ?></h1>
                </div>
                <!-- List Inline-->
                <ul class="list-inline list-inline-dashed list-white text-big p offset-md-top-13">
                  <li><a href="<?php bloginfo('url'); ?>">Home</a></li>
                  <li><?php the_title(); ?>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>