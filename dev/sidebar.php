<!-- Aside-->
                <aside class="inset-lg-left-20">
                  <!-- busca-->
                  <form class="form-search rd-search rd-search-sidebar" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
                    <div class="form-wrap">
                      <input type="text" class="form-search-input form-input" value="<?php the_search_query(); ?>" name="s" id="s" autocomplete="off" placeholder="Busca...">
                    </div>
                    <button class="form-search-submit" type="submit"><span class="icon icon-xs fa fa-search text-gray"></span></button>
                  </form>

                  <!-- sobre -->
                  <?php query_posts('pagename=sobre'); the_post(); ?>
                  <div class="post-content offset-top-34 offset-md-top-50">
                    <h3>Sobre</h3>
                    <p class="offset-top-13"><?php echo excerpt(27); ?></p><a class="btn btn-primary offset-top-20" href="<?php the_permalink(); ?>">Leia mais</a>
                  </div>
                  <?php wp_reset_query(); ?>

                  <!-- categorias -->
                  <?php $args = array('child_of' => 2);
                      $categories = get_categories( $args );
                      if ($categories) { ?>
                  <div class="offset-top-34 offset-md-top-50">
                    <h3>Categorias</h3>
                    <!-- List Marked-->
                    <ul class="list list-marked font-weight-bold offset-top-13">
                      <?php 
                      foreach($categories as $category) { 
                          echo '<li><a href="' . get_category_link( $category->term_id ) . '" class="nav-link" title="'. $category->name .'">● ' . $category->name.'</a></li> ';
                      } ?>
                    </ul>                    
                  </div>
                  <?php } wp_reset_query(); ?>

                  <!-- posts relacionados -->
                  <?php $orig_post = $post;
                  global $post;
                  $categories = get_the_category($post->ID);
                  if ($categories) {
                  $category_ids = array();
                  foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

                  $args=array(
                  'category__in' => $category_ids,
                  'post__not_in' => array($post->ID),
                  'posts_per_page'=> 3, // Number of related posts that will be shown.
                  'caller_get_posts'=>1
                  );

                  ?>
                  <?php $my_query = new wp_query( $args );
                  if( $my_query->have_posts() ) {  ?>
                  <div class="offset-top-34 offset-md-top-50">
                    <h3>Posts relacionados</h3>
                    <?php 
                    while( $my_query->have_posts() ) {
                    $my_query->the_post(); $altimg = get_the_title(); ?>
                    <div class="unit align-items-center flex-row unit-spacing-sm offset-top-13">
                      <div class="unit-left"><a class="thumbnail-classic" href="<? the_permalink()?>"><span class="thumbnail-classic-img-wrap"><?php 
                      the_post_thumbnail( array(80, 80, 'class' => 'img-fluid center-block', 'altimg' => $altimg))?></span></a></div>
                      <div class="unit-body">
                        <p class="font-weight-bold"><a class="link-decoration-none text-hover-primary text-gray" href="<? the_permalink()?>"><? the_title()?></a></p>
                        <!-- List Inline-->
                        <ul class="list-inline list-inline-dashed">
                          <li><?php the_time('j \d\e\ F, Y'); ?></li>                          
                        </ul>
                      </div>
                    </div>
                    <?php } ?>                    
                  </div>
                  <?php } ?>
                  <?php }
                  $post = $orig_post;
                  wp_reset_query(); ?>

                  <!-- arquivos  -->
                  <div class="offset-top-34 offset-md-top-50">
                    <h3>Arquivos</h3>
                    <!-- List Marked-->
                    <div class="list font-weight-bold offset-top-13">
                    <?php wp_get_archives(); ?>
                    </div>
                  </div>

                  <!-- tags cloud -->
                  <div class="offset-top-34 offset-md-top-50">
                    <h3>Nuvem de Tags</h3>
                    <div class="tags group-sm offset-top-13">
                      <?php wp_tag_cloud( array(
                       'smallest' => 14,
                       'largest'  => 22,
                       'unit'     => 'px',
                       'number'   => 45,
                       'orderby'  => 'name',
                       'order'    => 'ASC',
                       'taxonomy' => 'post_tag'
                      ) ); ?>
                    </div>
                  </div>
                  <?php /*
                  <div class="offset-top-34 offset-md-top-60"><a class="thumbnail-classic" href="#"><span class="thumbnail-classic-img-wrap"><img class="img-fluid center-block" src="images/banner-01-350x500.jpg" width="350" height="500" alt=""></span></a></div>*/ ?>
                </aside>