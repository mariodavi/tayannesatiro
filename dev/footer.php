      </main>
      
      <footer class="page-footer section-30 section-xs-15 bg-gray-lighter">
        <div class="container">
          <div class="row justify-content-sm-center align-items-sm-center">
            <div class="col-sm-7 text-sm-left">
              <p>Tayanne Sátiro &#169; <span class="copyright-year"></span>. Desenvolvido por <span class="link-decoration-none text-hover-primary text-gray-light" href="privacy-policy.html">ArtMídia Soluções</span></p>
            </div>
            <div class="col-sm-5 offset-top-10 offset-xs-top-0 text-sm-right">
              <!-- midias sociais-->
              <?php include('include/midias-sociais.php'); ?>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>

    <?php wp_footer(); ?>

    <?php if (is_home()) : ?>
    <script>
      $(document).ready(function(){
        $('.painel').slick({
          autoplay: true,
          autoplaySpeed: 3000,
        });
      });
    </script>
    <?php endif; ?>
    
    
    <!-- Global site tag (gtag.js) - Google Ads: 667455730 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-667455730"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-667455730'); </script>
    <!-- Event snippet for Website sale conversion page --> <script> gtag('event', 'conversion', { 'send_to': 'AW-667455730/o8QaCK_RyNMBEPKhor4C', 'transaction_id': '' }); </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163824760-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-163824760-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '353209755578969');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=353209755578969&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
	
  </body>
</html>