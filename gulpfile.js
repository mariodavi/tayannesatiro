// ////////////////////////////////////////////////////////////////
// NODE PACKAGES
// ////////////////////////////////////////////////////////////////

const babel       = require('gulp-babel');
      browserSync = require('browser-sync');
      buffer      = require('vinyl-buffer');
      concat      = require('gulp-concat');
      fileinclude = require('gulp-file-include');
      gulp        = require('gulp');
      htmlmin     = require('gulp-htmlmin');
      imagemin    = require('gulp-imagemin');
      merge       = require('merge-stream');
      newer       = require('gulp-newer');
      notify      = require('gulp-notify');
      plumber     = require('gulp-plumber');
      rename      = require('gulp-rename');
      sass        = require('gulp-sass');
      source      = require('vinyl-source-stream');
      sourcemaps  = require('gulp-sourcemaps');
      spritesmith = require('gulp.spritesmith');
      uglify      = require('gulp-uglify');
      vftp        = require('vinyl-ftp');
      gutil       = require('gulp-util');
      reload      = browserSync.reload;

// ////////////////////////////////////////////////////////////////
// PATHS
// ////////////////////////////////////////////////////////////////

const HTML_DEV_PATH = 'dev/*.php', //*.html
      //HTML_DEV_PATH = 'wordpress/wordpress-content/themes/tayannesatiro/',
      SASS_PATH     = 'dev/sass/**/*.scss',
      JS_DEV_PATH   = 'dev/js/*.js',
      SPRITE_PATH   = 'dev/sprite/*.+(png|jpg|jpeg|gif|svg)',
      IMGS_DEV_PATH = 'dev/images/**/*.+(png|jpg|jpeg|gif|svg)',

      /*HTML_PATH = 'site/',
      CSS_PATH  = 'site/css/', //'site/css/'
      JS_PATH   = 'site/js/',
      IMGS_PATH = 'site/img/';*/

      HTML_PATH = 'wordpress/wp-content/themes/tayannesatiro/',
      CSS_PATH  = 'wordpress/wp-content/themes/tayannesatiro/',
      JS_PATH   = 'wordpress/wp-content/themes/tayannesatiro/js/',
      IMGS_PATH = 'wordpress/wp-content/themes/tayannesatiro/images/';


// ////////////////////////////////////////////////////////////////
// SASS / CSS MINIFY TASK
// ////////////////////////////////////////////////////////////////

const sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

function css() {
  return gulp
    .src(SASS_PATH)
    //.pipe(sourcemaps.init({largeFile: true}))
    .pipe(sass(sassOptions).on('error', sass.logError))
    /*.pipe(sourcemaps.write('/', {
      addComment: false,
      includeContent: false,
      sourceRoot: CSS_PATH
    }))*/
    .pipe(gulp.dest(CSS_PATH))
    .pipe(browserSync.reload({stream:true}));
}

// ////////////////////////////////////////////////////////////////
// JS MINIFY TASK
// ////////////////////////////////////////////////////////////////

function scripts() {
	//return gulp.src(JS_DEV_PATH)
	return gulp.src([
		'dev/js/vendor/core.min.js',
    'dev/js/vendor/scripts.js',		
    'dev/js/vendor/slick.js',	
		'dev/js/scripts.js',
	])
	/*.pipe(sourcemaps.init({largeFile: true}))
	.pipe(babel({
		presets: ['@babel/preset-env']
	}))*/
	//.pipe(babel())
	.pipe(plumber())
	.pipe(concat('scripts.js'))
	.pipe(uglify())
	.pipe(rename(function (path) {
		path.basename += ".min"
	}))
	/*.pipe(sourcemaps.write('/', {
		addComment: false,
		includeContent: false,
		sourceRoot: JS_PATH
	}))*/
	.pipe(gulp.dest(JS_PATH))
	.pipe(browserSync.reload({stream:true}));
}

// ////////////////////////////////////////////////////////////////
// COMPRESS - SPRITE IMGS TASK
// ////////////////////////////////////////////////////////////////

function images() {
  return gulp.src(IMGS_DEV_PATH)
    .pipe(newer(IMGS_PATH))
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.mozjpeg({quality: 75, progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
          plugins: [
              {removeViewBox: true},
              {cleanupIDs: false}
          ]
      })
    ]))
    .pipe(gulp.dest(IMGS_PATH));
}

/* PROCURAR MODELO DO SPRITE EM NODE MODULES: scss.template.handlebars

SUBTITUIR CONTEÚDO POR: 

{{#sprites}}
.{{name}} {
  display: block;
  background-image: url({{{escaped_image}}});
  background-position: {{px.offset_x}} {{px.offset_y}};
  width: {{px.width}};
  height: {{px.height}};
}
{{/sprites}}
function sprite() {
  const data = gulp.src(SPRITE_PATH)
  .pipe(spritesmith({
    imgName: 'sprite.png',
    imgPath: 'img/sprite.png',
    cssName: '_sprite.scss'
  }));

  const imgStream = data.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(IMGS_PATH));

  const cssStream = data.css
    .pipe(gulp.dest('dev/scss'));

  return merge(imgStream, cssStream);
} */

// ////////////////////////////////////////////////////////////////
// SVG SPRITE GENERATE
// ////////////////////////////////////////////////////////////////

// npm install -g svg-sprite-generator
// svg-sprite-generate -d dev/img/svg -o site/img/svg-sprite.svg

// ////////////////////////////////////////////////////////////////
// COPYING BUILD FILES
// ////////////////////////////////////////////////////////////////

function extras() {
  return gulp.src([
    'dev/**/*',
    '!dev/sass{,/**/*}',
    '!dev/js/vendor{,/**/*}',
    '!dev/js/scripts.js',
    //'!dev/**/*.php' //*.php
  ], {
    dot: true
  }).pipe(gulp.dest(HTML_PATH));
}

// ////////////////////////////////////////////////////////////////
// HTML MINIFY TASK
// ////////////////////////////////////////////////////////////////

// https://goede.site/transpile-and-minify-javascript-html-and-css-using-gulp-4

function html() {
  return gulp
    .src(HTML_DEV_PATH)
    /*.pipe(fileinclude({ //include html files
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(htmlmin({ collapseWhitespace: true }))*/
    .pipe(gulp.dest(HTML_PATH));
}

// ////////////////////////////////////////////////////////////////
// BUILD POR TASK
// ////////////////////////////////////////////////////////////////

exports.html = html;
exports.images = images;
//exports.sprite = sprite;
exports.css = css;
exports.scripts = scripts;
exports.extras = extras;
exports.serve = serve;
//exports.deploy = deploy;
//exports.autodeploy = autodeploy;

// ////////////////////////////////////////////////////////////////
// BUILD TASKS
// ////////////////////////////////////////////////////////////////

exports.default = exports.build = gulp.parallel(html, css, scripts, images, /*sprite,*/ extras);

//exports.deploy = gulp.series(gulp.parallel(html, css, scripts, images, /*sprite,*/ extras),deploy);

// ////////////////////////////////////////////////////////////////
// BROWSER-SYNC / WATCH - RELOAD BROWSERS
// ////////////////////////////////////////////////////////////////

function serve() {
  //conn = getFtpConnection()
  
  browserSync.init({
    proxy:'http://localhost/tayannesatiro/wordpress/',
    port: 8080,
    open: true,
    notify: true
  });
  gulp.watch(SASS_PATH, css);
  gulp.watch(JS_DEV_PATH, scripts);
  //gulp.watch(SPRITE_PATH, sprite);
  gulp.watch(IMGS_DEV_PATH, images);
  gulp.watch(HTML_DEV_PATH, html);
  gulp.watch(HTML_DEV_PATH).on('change', browserSync.reload);

}

// ////////////////////////////////////////////////////////////////
// DEPLOY TASKS
// ////////////////////////////////////////////////////////////////

/*gulpftp = require('./config.js');

function getFtpConnection() {
  return vftp.create({
    host: host,
    port: port,
    user: user,
    pass: pass,
    //secure: true,
    debug: true,
    parallel: 5,
    log: gutil.log,
  })
}

function deploy() {

  conn = getFtpConnection()

  return gulp
    .src(localFilesGlob, {base: HTML_PATH, buffer: false })
    .pipe(conn.newer(remoteFolder)) // only upload newer files
    .pipe(conn.dest(remoteFolder))
    .pipe(notify("Arquivo publicado!"));
}*/