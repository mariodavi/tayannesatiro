# Tema do site Tayanne Sátiro - Nutricionista

## Build

Execute os seguintes comandos para gerar o site em `wordpress/`:

	% npm install
	% npm run build

## Develop

Para desenvolver localmente, execute os seguintes comandos:

	% npm install
	% npm run build
	% npm run dev